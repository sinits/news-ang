const express = require('express');
const router = express.Router();
const users = require('../controllers/userController');

router.get('/', users.getUsersList);
router.post('/create', users.createUser);
router.get('/get/:id', users.getUsersById);
router.post('/new', users.saveUser);
router.get('/getByToken/:id', users.isNewUser);
router.post('/update/:id', users.updateUser)

module.exports = router;