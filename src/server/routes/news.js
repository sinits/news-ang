const express = require('express');
const router = express.Router();
const newsController = require('../controllers/newsController');

router.get('/', newsController.getNewsList);
router.post('/create', newsController.createNews);
router.get('/get/:id', newsController.getNewsById);

module.exports = router;