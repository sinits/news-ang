'use strict';
module.exports = (sequelize, DataTypes) => {
  var Like = sequelize.define('Like', {
    userId: DataTypes.INTEGER,
    commentId:DataTypes.INTEGER
  });
  return Like;
};