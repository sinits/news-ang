'use strict';
module.exports = (sequelize, DataTypes) => {
  var NewsTag = sequelize.define('NewsTag', {
    newsId: DataTypes.INTEGER,
    TagId:DataTypes.INTEGER
  });
  return NewsTag;
};