'use strict';
module.exports = (sequelize, DataTypes) => {
  var News = sequelize.define('News', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    section: DataTypes.STRING,
    image_news: DataTypes.STRING,
    rating: DataTypes.FLOAT,
    UserID: DataTypes.FLOAT
  });
  News.associate = models => {
    News.belongsTo(models.User);
    News.hasMany(models.Comment);
    News.hasMany(models.Chapter);
    News.belongsToMany(models.Tag, {through: 'NewsTag'})
  };
  return News;
};