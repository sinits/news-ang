'use strict';
module.exports = (sequelize, DataTypes) => {
  var Chapter = sequelize.define('Chapter', {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    image_chapter: DataTypes.STRING,
    rating: DataTypes.STRING
  });
  Chapter.associate = models => {
       Chapter.belongsTo(models.News);
  };
  return Chapter;
};