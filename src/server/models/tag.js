'use strict';
module.exports = (sequelize, DataTypes) => {
  var Tag = sequelize.define('Tag', {
    value: DataTypes.STRING
  }, {});
  Tag.associate = models => {
    Tag.belongsToMany(models.News, {through: 'NewsTag'})
  };
  return Tag;
};