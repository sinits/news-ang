'use strict';
module.exports = (sequelize, DataTypes) => {
  var Comment = sequelize.define('Comment', {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    likes: DataTypes.INTEGER,
    UserId:DataTypes.INTEGER,
    NewsId:DataTypes.INTEGER
  });
  Comment.associate = models => {
    Comment.belongsTo(models.User);
    Comment.belongsTo(models.News);
    // Comments.belongsToMany(models.User, {through: 'Like'})
    // associations can be defined here
  };
  return Comment;
};