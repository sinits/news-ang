'use strict';
module.exports = (sequelize, DataTypes) => {
  var Rating = sequelize.define('Rating', {
    userId: DataTypes.INTEGER,
    chapterId: DataTypes.INTEGER,
    rating: DataTypes.INTEGER
  });
  return Rating;
};