'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    idToken: DataTypes.STRING,
    name: DataTypes.STRING,
    emails: DataTypes.STRING,
    password :DataTypes.STRING,
    image_user:DataTypes.STRING,
    admin: DataTypes.BOOLEAN,
    writer: DataTypes.BOOLEAN,
    verified_email: DataTypes.BOOLEAN,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    description: DataTypes.STRING
  });
  
  User.associate = models => {
    User.hasMany(models.News);
    User.hasMany(models.Comment);
  //  User.belongsToMany(models.Comments, {through: 'Like'})
  };                                                                                                                                                                                                                                                                                                                                                                                                                

  User.beforeCreate((user, options) => {
    user.password = bcrypt.hashSync(user.password, 8);
  })

  return User;
};