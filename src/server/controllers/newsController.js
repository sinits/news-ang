const models = require('../models');

const sendJSONResponse = (res, code, content) => {
    res.status(code).json(content);
};

module.exports.getNewsList = (req, res) => {
    models.News.findAll({}).then(news => {
        sendJSONResponse(res, 200, news);
    });
};
module.exports.getNewsById = (req, res) => {
    models.News.findById(req.params.id).then((news) => {
        res.json(news);
    });
};

module.exports.createNews = (req, res) => {
    models.News.create({
        title: 'Good News',
        description: 'many info',
        section: 'politic',
        image_news: 'url_image',
        rating: 3.5,
        UserID: 4
    }).then(news => {
        sendJSONResponse(res, 200, news);
    });
};