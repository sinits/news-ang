const models = require('../models');

const sendJSONResponse = (res, code, content) => {
    res.status(code).json(content);
};

module.exports.getUsersList = (req, res) => {
    models.User.findAll({}).then(users => {
        sendJSONResponse(res, 200, users);
    });
};
module.exports.getUsersById = (req, res) => {
    models.User.findById(req.params.id).then((user) => {
        res.json(user);
    });
};
module.exports.saveUser = (req, res) => {
    console.log(req.body);
    models.User.create({
        idToken: req.body.idToken,
        name: req.body.name,
        emails: req.body.emails,
        password: req.body.password,
        image_user: req.body.image_user,
        admin: req.body.admin,
        writer: req.body.writer,
        verified_email: req.body.verified_email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        description: req.body.description
    })
        .then(user => res.json(user));
}
module.exports.isNewUser = (req, res) => {
    models.User
        .findOne({ where: { idToken: req.params.id } }).then((result) => {
            if (result) {
                res.json(result);
            } else {
                this.createUser(req.params.id)
                    .then((data) => res.json(data))
                    .catch((err) => console.log(err))
            }
        })
}
module.exports.updateUser = (req, res) => {
    models.User.findById(req.params.id).then(user => {
        if (user) {
            user.update({
                name: req.body.name,
                emails: req.body.emails,
                password: req.body.password,
                image_user: req.body.image_user,
                admin: req.body.admin,
                writer: req.body.writer,
                verified_email: req.body.verified_email,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                description: req.body.description
            })
            res.json(user);
        }
    })
}

module.exports.deleteUser = (req, res) => {
    models.User.findById(req.params.id).then(user => {
        if (user) {
            user.destroy().then(user => {
                sendJSONResponse(res, 200, user);
            }).catch(err => {
                sendJSONResponse(res, 400, err.errors);
            });
        } else {
            sendJSONResponse(res, 404, [{ message: "user not found" }]);
        }
    })
        .catch(err => {
            sendJSONResponse(res, 400, err.errors);
        })
}

module.exports.createUser = (idToken) => {
    return models.User.create({
        idToken: idToken,
        name: '',
        emails: '',
        password: '',
        image_user: '',
        admin: false,
        writer: false,
        verified_email: false,
        firstname: '',
        lastname: '',
        description: ''
    }).then(user => user)
        .catch((err) => {
            throw err;
        });
};