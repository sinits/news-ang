const models = require('../models');

const sendJSONResponse = (res, code, content) => {
    res.status(code).json(content);
};

module.exports.getCommentsList = (req, res) => {
    models.Comment.findAll({}).then(comments => {
        sendJSONResponse(res, 200, comments);
    });
};

module.exports.createComment = (req, res) => {
    models.Comment.create({
        title: 'Good Ne',
        content: 'many info',
        likes: 5,
        UserId: 1,
        NewsId: 1
    }).then(comments => {
        sendJSONResponse(res, 200, comments);
    });
};