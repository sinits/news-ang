const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const users = require('./routes/users');
const news = require('./routes/news');
const comments = require('./routes/comments');
const app = express();
const port = 8000;

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(cors());
app.use('/users', users);
app.use('/news', news);
app.use('/comments', comments)

app.listen(port, () => {
    console.log(`Server listen on port ${port}...`);
});