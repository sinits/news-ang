import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CommentsService {

    private baseUrl = 'http://localhost:8000/comments';

    constructor(private http: HttpClient) {
    }

    // getUser(): Observable<any> {
    //     return this.http.get(`${this.baseUrl}/get/${1}`, httpOptions)
    // }
    

    getAllComments(): Observable<any> {

        // this.http.get(`${this.baseUrl}`, httpOptions).subscribe(
        //     data => { console.log(data) },
        //     err => { console.log('Not connectin with BASKEND', err.message) },
        //     () => { }
        // );
        return this.http.get<any>(`${this.baseUrl}`, httpOptions);
    }


}

