import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { UserModel } from '../model/user.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

    private baseUrl = 'http://localhost:8000/users';

    constructor(private http: HttpClient) {
    }

    getUser(id: number): Observable<any> {
        return this.http.get(`${this.baseUrl}/get/${id}`, httpOptions)
    }

    updateUser(profile : UserModel): Observable<any>{
        return this.http.post(`${this.baseUrl}/update/${profile.idToken}`,JSON.stringify(profile), httpOptions)
    }

    getByToken(id:string): Observable<any> {
        return this.http.get(`${this.baseUrl}/getByToken/${id}`, httpOptions)
    }
    

    getAllUsers(): Observable<any> {

        // this.http.get(`${this.baseUrl}`, httpOptions).subscribe(
        //     data => { console.log(data) },
        //     err => { console.log('Not connectin with BASKEND', err.message) },
        //     () => { }
        // );
        return this.http.get<any>(`${this.baseUrl}`, httpOptions);
    }


}

