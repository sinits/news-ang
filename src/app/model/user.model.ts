export class UserModel {
  id: number;
  idToken: string;
  name: string;
  emails: string;
  password: string;
  image_user: string;
  admin: boolean;
  writer: boolean;
  verified_email: boolean;
  firstname: string;
  lastname: string;
  discription: string;
}
