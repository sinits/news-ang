import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { UserModel } from '../../model/user.model';
import { UserService } from '../../service/user.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  profile: UserModel = {
    id: 0,
    idToken: '',
    name: '',
    emails: '',
    password: '',
    image_user: '',
    admin: false,
    writer: false,
    verified_email: false,
    firstname: '',
    lastname: '',
    discription: '',
  };
  authInfo: any;
  user: any;
  constructor(public auth: AuthService, private uService: UserService) { }

  updateUser(){
    this.uService.updateUser(this.profile).subscribe(
      data => {
        this.profile = data as UserModel;
      },
      err => { console.log('Not connectin with BASKEND', err.message) },
      () => { });
  }

  userProfile() {
    {
      if (this.auth.userProfile) {
        this.authInfo = this.auth.userProfile;
        this.getByToken(this.authInfo.sub);
        console.log("syb      ", this.authInfo.sub);
      } else {
        this.auth.getProfile((err, authInfo) => {
          this.authInfo = authInfo;
          this.getByToken(this.authInfo.sub);
          console.log("syb      ", this.authInfo.sub);
        });
      }
    }
  }
  getByToken(token: string) {
    this.uService.getByToken(token).subscribe(
      data => {
        console.log(data);
        this.profile = data as UserModel;;
      },
      err => { console.log('Not connectin with BASKEND', err.message) },
      () => { });
  }
  ngOnInit() {
    this.userProfile();
  }
}
