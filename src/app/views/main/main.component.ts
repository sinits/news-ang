import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { UserService } from "../../service/user.service";
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  user: any;
  constructor(private router: Router, public auth: AuthService, private uService: UserService) {

  }
  getServer() {
    console.log("check");
    this.router.navigate(['/user']);
    // this.uService.getUser().subscribe(
    //   data => {
    //     console.log(data);
    //     this.user = data;
    //   },
    //   err => { console.log('Not connectin with BASKEND', err.message) },
    //   () => { });

    // this.uService.getAllUsers();
  }
  getUserAll() {
    console.log("allcheck");
    this.uService.getAllUsers().subscribe(
      data => { console.log(data);
      this.user = data },
      err => { console.log('Not connectin with BASKEND', err.message) },
      () => { });
  }
  

  ngOnInit() {
    // this.getServer();
    this.getUserAll();
  }

}
