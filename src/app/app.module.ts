import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

// import { ROUTES } from './app.routes';
import { AuthService } from './auth/auth.service';
import { CallbackComponent } from './callback/callback.component';
import { UserComponent } from './views/user/user.component';
import { MainComponent } from './views/main/main.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from "../app/service/user.service";
import {routing, appRoutingProviders} from './app.routes';
import { NewsListComponent } from "./news-list/news-list.component";
import { NewsService } from './service/news.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CallbackComponent,
    UserComponent,
    MainComponent,
    NewsListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    routing
  ],
  providers: [appRoutingProviders, AuthService, UserService, NewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
