import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
// import { CommentsService } from '../service/comment.service';
// import { UserService } from '../service/user.service';
import { NewsService } from '../service/news.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {
  news: any;
  // constructor(){}
  constructor(private router: Router, public auth: AuthService, private uService: NewsService) { }
  // getAllNews() {
  //   this.uService.getAllComments().subscribe(
  //     data => { console.log(data);
  //     this.news = data },
  //     err => { console.log('Not connectin with BASKEND', err.message) },
  //     () => { });

  //   // this.uService.getAllNews();
  // }
  getUserAll() {
    console.log("allcheck");
    this.uService.getAllNews().subscribe(
      data => { console.log(data);
      this.news = data },
      err => { console.log('Not connectin with BASKEND', err.message) },
      () => { });
  }
  ngOnInit() {
    this.getUserAll();
  }

}
