import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CallbackComponent } from './callback/callback.component';
import { UserComponent } from './views/user/user.component';
import { NewsListComponent } from './news-list/news-list.component';


export const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'callback', component: CallbackComponent },
  // { path: '**', redirectTo: '' },
  { path: 'newslist', component: NewsListComponent},
  { path: 'user', component: UserComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
export const appRoutingProviders: any[] = [];
// export const RoutesModule: ModuleWithProviders = RouterModule.forRoot(router);